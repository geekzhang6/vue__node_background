const express = require('express');
//使用路由
const router = express.Router();
const passport = require('passport');
//认证
const authenticate = passport.authenticate('jwt', {session: false});

const Profile = require('../../models/Profile');

/**
 * $route GET /api/profiles/test
 * @desc 返回请求的json数据
 * @access public
 */
router.get("/test", (req, res) => {
    res.json({msg: "Profile works"});
});


/**
 * $route Post /api/profiles/add
 * @desc 创建信息接口
 * @return
 * @access private
 */
router.post('/add', authenticate, (req, res) => {
    const profileFields = {};

    if (req.body.type) profileFields.type = req.body.type;
    if (req.body.describe) profileFields.describe = req.body.describe;
    if (req.body.income) profileFields.income = req.body.income;
    if (req.body.expend) profileFields.expend = req.body.expend;
    if (req.body.cash) profileFields.cash = req.body.cash;
    if (req.body.remark) profileFields.remark = req.body.remark;

    new Profile(profileFields).save().then(profiles => {
        return res.json(profiles);
    }).catch(err => {
        return res.json(err);
    })

});


/**
 * $route GET /api/profiles/
 * @desc 获取所有信息
 * @return 所有信息
 * @access private
 */
router.get('/', authenticate, (req, res) => {
    Profile.find()
        .then(profile => {
            if (!profile) {
                return res.status(404).json("没有任何内容");
            }
            res.json(profile);
        })
        .catch(err => res.status(404).json(err));
});

/**
 * $route GET /api/profiles/:id
 * @desc 获取所有一条信息
 * @return 所有信息
 * @access private
 */
router.get('/:id', authenticate, (req, res) => {
    Profile.findOne({_id: req.params.id})
        .then(profile => {
            if (!profile) {
                return res.status(404).json("没有任何内容");
            }
            res.json(profile);
        })
        .catch(err => res.status(404).json(err));
});


/**
 * $route GET /api/profiles/edit
 * @desc 编辑信息
 * @return
 * @access private
 */
router.post('/edit/:id', authenticate, (req, res) => {
    const profileFields = {};

    if (req.body.type) profileFields.type = req.body.type;
    if (req.body.describe) profileFields.describe = req.body.describe;
    if (req.body.income) profileFields.income = req.body.income;
    if (req.body.expend) profileFields.expend = req.body.expend;
    if (req.body.cash) profileFields.cash = req.body.cash;
    if (req.body.remark) profileFields.remark = req.body.remark;

    Profile.findOneAndUpdate(
        {_id: req.params.id}, {$set: profileFields}, {new: true})
        .then(profiles => {
            return res.json(profiles);
        }).catch(err => {
        return res.json(err);
    })

});

/**
 * $route delete /api/profiles/delete
 * @desc 删除信息
 * @return
 * @access private
 */
router.delete('/delete/:id', authenticate, (req, res) => {
    Profile.findOneAndRemove({_id: req.params.id})
        .then(profile => {
            return res.json(profile);
        })
        .catch(err => res.status(404).json("删除失败"));
});


module.exports = router;
