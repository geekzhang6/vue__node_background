const mongoose = require('mongoose');
const schema = mongoose.Schema;
//创建规则
const ProfileSchema = new schema({
    type: {
        type: String
    },
    describe: {
        type: String
    },
    income: {
        type: String,
        required: true
    },
    expend: {
        type: String,
        required: true
    },
    cash: {
        type: String,
        required: true
    },
    remark: {
        type: String
    },
    Date: {
        type: Date,
        default: Date.now
    }

});


module.exports = Profile = mongoose.model("profile", ProfileSchema);



