const mongoose = require('mongoose');
const schema = mongoose.Schema;
//创建规则
const UserSchema = new schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    avatar: {
        type: String,
    },
    identity: {
        type: String,
        required:true
    },
    date: {
        type: Date,
        default: Date.now
    }
});


module.exports = User = mongoose.model("users", UserSchema);



