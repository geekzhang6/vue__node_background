//文档复制
const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

const mongoose = require('mongoose');
const User = mongoose.model("users");
const keys = require('../config/keys');
//文档复制
let opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = keys.secretOrKey;


module.exports = passport => {
    //文档复制
    passport.use(new JwtStrategy(opts, (jwt_payload, done) => {

        // console.log(jwt_payload); //包含前面rule里面设置的id和name等
        //通过rule里面的内容来进行身份登录认证
        User.findById(jwt_payload.id)
            .then(user => {
                if (user) {
                    return done(null, user);//认证成功 user会到req.user里面
                }
                return done(null, false);//认证失败
            })
            .catch(err => console.error(err));//发送错误
        // User.findOne({id: jwt_payload.sub}, function (err, user) {
        //
        //     // if (err) {
        //     //     return done(err, false);
        //     // }
        //     // if (user) {
        //     //     return done(null, user);
        //     // } else {
        //     //     return done(null, false);
        //     //     // or you could create a new account
        //     // }
        // });
    }));
};
