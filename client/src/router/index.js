import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from '../views/index'
// import Register from "../views/Register";
// import NotFound from "../views/404"
// import Login from "../views/Login";
// import Home from "../views/Home"
// import InfoShow from '../views/InfoShow'
// import Foundlist from "../views/FoundList";


const Foundlist = () => import('../views/FoundList')
const InfoShow = () => import('../views/InfoShow')
const Home = () => import('../views/Home')
const Login = () => import('../views/Login')
const NotFound = () => import('../views/404')
const Register = () => import('../views/Register')




Vue.use(VueRouter);

//设置路由规则
const routes = [
    {
        path: '/',
        redirect: '/index'
    },
    {
        path: '/index',
        name: 'index',
        component: Index,
        children:[
            {path:'',component: Home},
            {path:'/home',name:'home',component: Home},
            {path:'/infoshow',name:'infoshow',component: InfoShow},
            {path:'/foundlist',name:'foundlist',component: Foundlist}
        ]
    },
    {
        path: '/register',
        name: 'register',
        component: Register
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '*',
        name: '/404',
        component: NotFound
    }
];

// const router = new VueRouter({
//     mode: 'history',
//     base: process.env.BASE_URL,
//     routes
// });

// //路由守卫
// router.beforeEach((to, from, next) => {
//     //login之后存到localstorage中的Token
//     const isLogin = localStorage.eleToken ? true : false;
//     if (to.path == "/login" || to.path == "/register") {
//         next();
//     } else {
//         isLogin ? next() : next('/login');
//     }
// });


export function createRouter(){
    const router = new VueRouter({
        mode: 'history',
        base: process.env.BASE_URL,
        routes
    });

    //路由守卫
    router.beforeEach((to, from, next) => {
        //login之后存到localstorage中的Token
        const isLogin = localStorage.eleToken ? true : false;
        if (to.path == "/login" || to.path == "/register") {
            next();
        } else {
            isLogin ? next() : next('/login');
        }
    });

    return router;
}
