//通用
import Vue from 'vue'
import App from './App.vue'
import { createRouter } from './router/index'
import store from './store'
import axios from './http'

//引入elementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';



//使用ElementUI
Vue.use(ElementUI);

//配置到原型，即可全局使用
Vue.prototype.$axios = axios;

export function createApp() {
    const router = createRouter();

    const app = new Vue({
        router,
        store,
        render: h => h(App)
    })
    return { app, router }
}
