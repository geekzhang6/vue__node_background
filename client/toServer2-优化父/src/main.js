import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//请求
import axios from './http'

//引入elementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.config.productionTip = true;
//使用ElementUI
Vue.use(ElementUI);

//配置到原型，即可全局使用
Vue.prototype.$axios = axios;


new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
