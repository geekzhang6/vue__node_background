// const path = require('path');
// const debug = process.env.NODE_ENV !== 'production';

// module.exports = {
//     //baseUrl: '/', // 根域上下文目录
//     //outputDir: 'dist', // 构建输出目录
//     assetsDir: 'assets', // 静态资源目录 (js, css, img, fonts)
//     lintOnSave: false, // 是否开启eslint保存检测，有效值：ture | false | 'error'
//     runtimeCompiler: true, // 运行时版本是否需要编译
//     transpileDependencies: [], // 默认babel-loader忽略mode_modules，这里可增加例外的依赖包名
//     productionSourceMap: true, // 是否在构建生产包时生成 sourceMap 文件，false将提高构建速度
//     configureWebpack: config => { // webpack配置，值位对象时会合并配置，为方法时会改写配置
//         if (debug) { // 开发环境配置
//             config.devtool = 'cheap-module-eval-source-map'
//         } else { // 生产环境配置
//         }
//         // Object.assign(config, { // 开发生产共同配置
//         //     resolve: {
//         //         alias: {
//         //             '@': path.resolve(__dirname, './src'),
//         //             '@c': path.resolve(__dirname, './src/components'),
//         //             'vue$': 'vue/dist/vue.esm.js'
//         //         }
//         //     }
//         // })
//     },
//     chainWebpack: config => { // webpack链接API，用于生成和修改webapck配置，https://github.com/vuejs/vue-cli/blob/dev/docs/webpack.md
//         if (debug) {
//             // 本地开发配置
//         } else {
//             // 生产开发配置
//         }
//     },
//     parallel: require('os').cpus().length > 1, // 构建时开启多进程处理babel编译
//     pluginOptions: { // 第三方插件配置
//     },
//     pwa: { // 单页插件相关配置 https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-pwa
//     }
//     // devServer: {
//     //     open: true,
//     //     host: 'localhost',
//     //     port: 8080,
//     //     https: false,
//     //     hotOnly: false,
//     //     proxy: { // 配置跨域
//     //         '/api': {
//     //             target: 'http://127.0.0.1:5000/api/',
//     //             ws: true,
//     //             changOrigin: true,
//     //             pathRewrite: {
//     //                 '^/api': ''
//     //             }
//     //         }
//     //     },
//     //     before: app => {
//     //     }
//     // }
// }

// vue.config.js
const VueSSRServerPlugin = require("vue-server-renderer/server-plugin");
const VueSSRClientPlugin = require("vue-server-renderer/client-plugin");

const nodeExternals = require("webpack-node-externals");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin")
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin')
const CompressionWebpackPlugin = require("compression-webpack-plugin")

const merge = require("lodash.merge");

const TARGET_NODE = process.env.WEBPACK_TARGET === "node";
const target = TARGET_NODE ? "server" : "client";
module.exports = {
    css: {
        extract: true,
        sourceMap: false
    },
    outputDir: './dist/' + target,
    productionSourceMap: false,
    configureWebpack: () => ({
        // 将 entry 指向应⽤程序的 server / client ⽂件
        entry: TARGET_NODE ? `./src/entry-${target}.js` : `./src/entry-${target}.js`,

        // 对 bundle renderer 提供 source map ⽀持
        //devtool: 'source-map',
        target: TARGET_NODE ? "node" : "web",
        node: TARGET_NODE ? undefined : false,
        output: {
            libraryTarget: TARGET_NODE ? "commonjs2" : undefined,
        },
        // https://webpack.js.org/configuration/externals/#function
        // https://github.com/liady/webpack-node-externals
        // 外置化应⽤程序依赖模块。可以使服务器构建速度更快，
        // 并⽣成较⼩的 bundle ⽂件。
        externals: TARGET_NODE
            ? nodeExternals({
                // 不要外置化 webpack 需要处理的依赖模块。
                // 你可以在这⾥添加更多的⽂件类型。例如，未处理 *.vue 原始⽂件，
                // 你还应该将修改 `global`（例如 polyfill）的依赖模块列⼊⽩名单
                allowlist: [/\.css$/],

            })
            : undefined,
        optimization: {
            splitChunks: TARGET_NODE ? false : undefined,

        },
        plugins: [
            TARGET_NODE ? new VueSSRServerPlugin() : new VueSSRClientPlugin(),
            new OptimizeCSSPlugin(),
            new UglifyJsPlugin(
                {
                    sourceMap: false,
                    uglifyOptions: {
                        compress: true
                    }
                }),
            new CompressionWebpackPlugin({
                //deleteOriginalAssets: true
            })
        ]
    }),
    chainWebpack: config => {
        config.module
            .rule("vue")
            .use("vue-loader")
            .tap(options => {
                merge(options, {
                    optimizeSSR: false
                });
            });
    }
};
